<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use App\Product;

class ProductsController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth.basic.once');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $products = Product::all();

        return response()->json([ 'status' => 'ok', 'products' => $products], 200);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $values = $request->only(['name','price','quantity', 'description']);
        Product::create($values);

        return response()->json(['message' => 'Product Created Successfully', 'success' => 1], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if(!$product){
            return response()->json(['message' => 'This product does not exist','success'=> 0 , 'code' => 404], 404);
        }

        return response()->json(['data' => $product, 'success'=> 1 ], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(CreateProductRequest $request, $id)
    {
        $product = Product::find($id);

        if(!$product) {
            return response()->json(['message' => 'This product does not exist', 'success'=> 0,'code' => 404], 404);
        }

        $name = $request->get('name');
        $price = $request->get('price');
        $quantity = $request->get('quantity');
        $description = $request->get('description');


        $product->name = $name;
        $product->price = $price;
        $product->quantity = $quantity;
        $product->description = $description;

        $product->save();

        return response()->json(['message' => 'The Product has been updated', 'success'=> 1], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if(!$product)
        {
            return response()->json(['message' => 'This Product does not exist', 'success'=> 0, 'code' => 404], 404);
        }

        $product->delete();

        return response()->json(['message' => 'This Product has been deleted', 'success'=> 1,'code' => 200], 200);
    }
}
